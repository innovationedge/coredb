from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, Boolean, DateTime, MetaData, ForeignKeyConstraint

def create_core_entities(engine, metadata):
	value = Table('value', metadata,
		Column('id', String, primary_key=True),
		Column('type', String),
		Column('description', String)
	)
	model = Table('model', metadata, 
		Column('id', String, primary_key=True),
		Column('description', String)
	)
	transport = Table('transport', metadata, 
		Column('id', String, primary_key=True),
		Column('description', String)
	)
	argument = Table('argument', metadata, 
		Column('id', String, primary_key=True),
		Column('type', String),
		Column('description', String)
	)
	device = Table('device', metadata, 
		Column('id', String, primary_key=True)
	)
	platform = Table('platform', metadata, 
		Column('id', String, primary_key=True),
		Column('release', String)
	)
	return metadata

def create_core_relationships(engine, metadata):
	value_model = Table('value_model', metadata, 
		Column('value_id', None, primary_key=True),
		Column('model_id', None, primary_key=True),
		ForeignKeyConstraint(['value_id'], ['value.id'], ondelete='CASCADE', onupdate='CASCADE'),
		ForeignKeyConstraint(['model_id'], ['model.id'], ondelete='CASCADE', onupdate='CASCADE')
	)
	model_transport = Table('model_transport', metadata, 
		Column('model_id', None, primary_key=True),
		Column('transport_id', None, primary_key=True),
		ForeignKeyConstraint(['model_id'], ['model.id'], ondelete='CASCADE', onupdate='CASCADE'),
		ForeignKeyConstraint(['transport_id'], ['transport.id'], ondelete='RESTRICT', onupdate='CASCADE')
	)
	model_parent = Table('model_parent', metadata, 
		Column('model_id', None, primary_key=True),
		Column('model_parent_id', None, primary_key=True),
		ForeignKeyConstraint(['model_id'], ['model.id']),
		ForeignKeyConstraint(['model_parent_id'], ['model.id'], ondelete='CASCADE', onupdate='CASCADE')
	)
	model_argument = Table('model_argument', metadata, 
		Column('model_id', None, primary_key=True),
		Column('argument_id', None, primary_key=True),
		Column('position', Integer),
		ForeignKeyConstraint(['model_id'], ['model.id'], ondelete='CASCADE', onupdate='CASCADE'),
		ForeignKeyConstraint(['argument_id'], ['argument.id'], ondelete='RESTRICT', onupdate='CASCADE')
	)
	model_platform = Table('model_platform', metadata, 
		Column('model_id', None, primary_key=True),
		Column('platform_id', None, primary_key=True),
		ForeignKeyConstraint(['model_id'], ['model.id'], ondelete='CASCADE', onupdate='CASCADE'),
		ForeignKeyConstraint(['platform_id'], ['platform.id'], ondelete='RESTRICT', onupdate='CASCADE')
	)
	platform_device = Table('platform_device', metadata, 
		Column('platform_id', None, primary_key=True),
		Column('device_id', None, primary_key=True),
		ForeignKeyConstraint(['platform_id'], ['platform.id'], ondelete='RESTRICT', onupdate='CASCADE'),
		ForeignKeyConstraint(['device_id'], ['device.id'], ondelete='CASCADE', onupdate='CASCADE')
	)
	return metadata

def create_web_entities(engine, metadata):
	""" Create webdb entities. """
	user = Table('user', metadata, 
		Column('id', String, primary_key=True),
		Column('admin', Boolean, nullable=False)
	)
	group = Table('group', metadata, 
		Column('id', String, primary_key=True),
		Column('mailer', String)
	)
	ldapgroup = Table('ldapgroup', metadata, 
		Column('id', String, primary_key=True),
		Column('pending', Boolean, nullable=False)
	)
	# This needs to be examined...
	comment = Table('comment', metadata, 
		Column('id', Integer, primary_key=True),
		Column('timestamp', DateTime(timezone=True), nullable=False),
		Column('official', Boolean, nullable=False),
		Column('content', String, nullable=False)
	)
	issue = Table('issue', metadata, 
		Column('id', String, primary_key=True),
		Column('timestamp', DateTime(timezone=True), nullable=False),
		Column('description', String),
		Column('open', Boolean, nullable=False),
		Column('priority', Integer, nullable=False)
	)
	bugnumber = Table('bugnumber', metadata, 
		Column('id', String, primary_key=True)
	)
	issue_type = Table('type', metadata, 
		Column('id', String, primary_key=True)
	)

def create_web_relationships(engine, metadata):
	""" Create relationship tables between entities. """
	user_has_group = Table('user_has_group', metadata, 
		Column('user_id', None, primary_key=True),
		Column('group_id', None, primary_key=True),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['group_id'], ['group.id'], onupdate='CASCADE', ondelete='CASCADE')
	)
	user_invited_ldapgroup = Table('user_invited_ldapgroup', metadata, 
		Column('user_id', None, primary_key=True),
		Column('ldapgroup_id', None, primary_key=True),
		Column('timestamp', DateTime(timezone=True), nullable=False),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='RESTRICT'),
		ForeignKeyConstraint(['ldapgroup_id'], ['ldapgroup.id'], onupdate='CASCADE', ondelete='CASCADE')
	)
	user_added_ldapgroup = Table('user_authorized_ldapgroup', metadata, 
		Column('user_id', None, primary_key=True),
		Column('ldapgroup_id', None, primary_key=True),
		Column('timestamp', DateTime(timezone=True), nullable=False),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='RESTRICT'),
		ForeignKeyConstraint(['ldapgroup_id'], ['ldapgroup.id'], onupdate='CASCADE', ondelete='CASCADE')
	)
	user_author_comment = Table('user_author_comment', metadata, 
		Column('user_id', None, primary_key=True),
		Column('comment_id', None, primary_key=True),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='RESTRICT'),
		ForeignKeyConstraint(['comment_id'], ['comment.id'], onupdate='CASCADE', ondelete='CASCADE')
	)
	user_author_issue = Table('user_author_issue', metadata, 
		Column('user_id', None, primary_key=True),
		Column('issue_id', None, primary_key=True),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='RESTRICT'),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE')		
	)
	user_interest_issue = Table('user_interest_issue', metadata, 
		Column('user_id', None, primary_key=True),
		Column('issue_id', None, primary_key=True),
		Column('status', Boolean, nullable=False),
		ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE')		
	)
	issue_relates_group = Table('issue_relates_group', metadata, 
		Column('issue_id', None, primary_key=True),
		Column('group_id', None, primary_key=True),
		Column('timestamp', DateTime(timezone=True), nullable=False),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['group_id'], ['group.id'], onupdate='CASCADE', ondelete='RESTRICT')
	)
	issue_relates_device = Table('issue_relates_device', metadata, 
		Column('issue_id', None, primary_key=True),
		Column('device_id', None, primary_key=True),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['device_id'], ['device.id'], onupdate='CASCADE', ondelete='RESTRICT')
	)
	issue_relates_model = Table('issue_relates_model', metadata, 
		Column('issue_id', None, primary_key=True),
		Column('model_id', None, primary_key=True),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['model_id'], ['model.id'], onupdate='CASCADE', ondelete='RESTRICT')
	)
	issue_is_type = Table('issue_is_type', metadata, 
		Column('issue_id', None, primary_key=True),
		Column('type_id', None, primary_key=True),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='CASCADE'),
		ForeignKeyConstraint(['type_id'], ['type.id'], onupdate='CASCADE', ondelete='RESTRICT')
	)
	issue_has_bugnumber = Table('issue_has_bugnumber', metadata, 
		Column('issue_id', None, primary_key=True),
		Column('bugnumber_id', None, primary_key=True),
		ForeignKeyConstraint(['issue_id'], ['issue.id'], onupdate='CASCADE', ondelete='RESTRICT'),
		ForeignKeyConstraint(['bugnumber_id'], ['bugnumber.id'], onupdate='CASCADE', ondelete='CASCADE')
	)

def setup_db():
	print('Creating database...')
	engine = create_engine('sqlite:///utdm.db', echo=True)
	metadata = MetaData()
	create_core_entities(engine, metadata)
	print('Core entities created...')
	create_core_relationships(engine, metadata)
	print('Core relationships created.')
	create_web_entities(engine, metadata)
	print('Web entities created...')
	create_web_relationships(engine, metadata)
	print('Web relationships created...')
	metadata.create_all(engine)
	print('Database committed...')
	print('Database creation finished.')

if __name__ == '__main__':
	setup_db()
