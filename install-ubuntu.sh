#!/bin/bash

sudo apt-get update
sudo apt-get install build-essential python3 python-dev python-pip sqlite
sudo pip install pip --upgrade
sudo pip install urllib3[secure] --upgrade
sudo pip install sqlite3
